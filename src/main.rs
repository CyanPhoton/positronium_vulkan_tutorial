use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{Read, Seek, SeekFrom};
use std::ops::{Deref, DerefMut};
use std::path::Path;
use std::sync::Arc;
use std::thread::{JoinHandle, spawn};
use std::time::{Duration, Instant};
use parking_lot::RwLock;

use image::EncodableLayout;
use positronium_maths as psm;
use positronium_maths::{ControlAxis, DepthDomain, DepthRange, ForwardDirection, NdcConfig, Vector3usize};
use positronium_vk as vk;
use winit::dpi::PhysicalSize;
use winit::event::{Event, VirtualKeyCode, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::{Fullscreen, Window, WindowBuilder};

fn main() {
    let init_window_size = PhysicalSize::new(1920, 1080);

    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title("Vulkan")
        .with_inner_size(init_window_size)
        .with_resizable(true)
        .build(&event_loop).unwrap();

    let mut app = HelloTriangleApplication::new(&window, init_window_size);

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Wait;

        match event {
            Event::WindowEvent {
                window_id, event
            } => {
                match event {
                    WindowEvent::CloseRequested => {
                        if window.id() == window_id {
                            *control_flow = ControlFlow::Exit;
                            app.send_close_request();
                        }
                    }
                    WindowEvent::Resized(s) => {
                        app.send_resize(s);
                    }
                    WindowEvent::ScaleFactorChanged { .. } => {}
                    WindowEvent::KeyboardInput { device_id: _, input, is_synthetic: _ } => {
                        if let Some(vk) = input.virtual_keycode {
                            if vk == VirtualKeyCode::F11 {
                                window.set_fullscreen(match window.fullscreen() {
                                    None => {
                                        Some(Fullscreen::Exclusive(window.current_monitor().unwrap().video_modes().filter(|v| v.refresh_rate() < 130).next().unwrap()))
                                    }
                                    Some(fullscreen) => {
                                        match fullscreen {
                                            Fullscreen::Exclusive(_v) => {
                                                Some(Fullscreen::Borderless(window.current_monitor()))
                                            }
                                            Fullscreen::Borderless(_m) => {
                                                None
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                    _ => {}
                }
            }
            Event::LoopDestroyed => {
                app.clean_up();
            }
            _ => {}
        }
    })
}

enum WindowMessage {
    Resize(PhysicalSize<u32>),
    CloseRequest,
}

struct HelloTriangleApplication {
    window_message_sender: flume::Sender<WindowMessage>,
    thread: Option<JoinHandle<()>>,
}

impl HelloTriangleApplication {
    pub fn new(window: &Window, window_size: PhysicalSize<u32>) -> Self {
        let (window_message_sender, window_message_receiver) = flume::unbounded::<WindowMessage>();

        let mut data = HelloTriangleApplicationData::new();
        data.init_vulkan(window, window_size);

        let thread = std::thread::spawn(move || {
            let window_message_receiver = window_message_receiver;
            let mut data = data;

            data.last_window_size = window_size;
            let mut need_recreate_swapchain = false;

            let target_fps = 240.0f32;
            let target_interval = Duration::from_secs_f32(target_fps.recip());
            let mut last_frame = Instant::now();

            'main_loop: loop {
                while let Ok(message) = window_message_receiver.try_recv() {
                    match message {
                        WindowMessage::Resize(resize) => {
                            data.last_window_size = resize;
                            need_recreate_swapchain = true;
                        }
                        WindowMessage::CloseRequest => {
                            break 'main_loop;
                        }
                    }
                }
                if need_recreate_swapchain {
                    need_recreate_swapchain = data.recreate_swapchain();
                    continue;
                }

                match data.main_loop() {
                    MainLoopResult::OutdatedSwapchain => need_recreate_swapchain = true,
                    MainLoopResult::Skipped => {
                        std::thread::yield_now();
                    }
                    MainLoopResult::Success => {
                        let fps = (data.present_times.iter().skip(1).zip(data.present_times.iter()).map(|(t0, t1)| (*t1 - *t0).as_secs_f32()).sum::<f32>()
                            / (data.present_times.len() - 1) as f32).recip();
                        // println!("FPS: {}", fps);

                        if let Some(d) = target_interval.checked_sub(Instant::now() - last_frame) {
                            std::thread::sleep(d);
                        }
                        last_frame = Instant::now();
                    }
                }
            }

            data.clean_up();
        });

        HelloTriangleApplication {
            window_message_sender,
            thread: Some(thread),
        }
    }

    pub fn send_resize(&self, size: PhysicalSize<u32>) {
        self.window_message_sender.send(WindowMessage::Resize(size)).expect("Failed to send resize update");
    }

    pub fn send_close_request(&self) {
        self.window_message_sender.send(WindowMessage::CloseRequest).expect("Failed to send close request")
    }

    pub fn clean_up(&mut self) {
        self.thread.take().unwrap().join().unwrap();
    }
}

#[derive(Clone)]
struct BaseVulkanObjects {
    loader: Arc<vk::Loader>,
    instance: Arc<vk::Instance>,
    physical_device: Arc<vk::PhysicalDevice>,
    queue_family_indices: QueueFamilyIndices,
    device: Arc<RwLock<vk::Device>>,
    graphics_queue: Arc<RwLock<vk::Queue>>,
    present_queue: Arc<RwLock<vk::Queue>>,
    msaa_samples: vk::SampleCountFlagBits,
    descriptor_set_layout: Arc<vk::DescriptorSetLayout>,
    texture_image: Arc<vk::Image>,
    texture_image_memory: Arc<vk::DeviceMemory>,
    texture_image_view: Arc<vk::ImageView>,
    texture_sampler: Arc<vk::Sampler>,
    vertex_buffer: Arc<vk::Buffer>,
    vertex_buffer_memory: Arc<vk::DeviceMemory>,
    index_buffer: Arc<vk::Buffer>,
    index_buffer_memory: Arc<vk::DeviceMemory>,
    index_size: usize
}

struct HelloTriangleApplicationData {
    base_objects: BaseVulkanObjects,
    // loader: vk::Loader,
    // instance: vk::Instance,
    debug_messenger: Option<vk::DebugUtilsMessengerEXT>,
    // physical_device: vk::PhysicalDevice,
    // queue_family_indices: QueueFamilyIndices,
    // device: vk::Device,
    // graphics_queue: vk::Queue,
    // present_queue: vk::Queue,
    surface: Arc<RwLock<vk::SurfaceKHR>>,
    swapchain: Arc<RwLock<vk::SwapchainKHR>>,
    swapchain_image_format: vk::Format,
    swapchain_extent: vk::Extent2D,
    swapchain_images: Vec<vk::Image>,
    swapchain_image_views: Vec<vk::ImageView>,
    render_pass: Arc<vk::RenderPass>,
    pipeline_layout: Arc<vk::PipelineLayout>,
    graphics_pipeline: Arc<vk::Pipeline>,
    swapchain_framebuffers: Vec<vk::Framebuffer>,
    command_pool: Arc<RwLock<vk::CommandPool>>,
    command_buffers: Vec<vk::CommandBuffer>,
    image_available_semaphores: [vk::Semaphore; Self::MAX_FRAMES_IN_FLIGHT],
    render_finished_semaphores: [vk::Semaphore; Self::MAX_FRAMES_IN_FLIGHT],
    in_flight_frames: [Arc<RwLock<vk::Fence>>; Self::MAX_FRAMES_IN_FLIGHT],
    images_in_flight: Vec<Option<Arc<RwLock<vk::Fence>>>>,
    current_frame: usize,
    acquired: Option<u32>,
    present_times: VecDeque<Instant>,
    vertices: Vec<Vertex>,
    indices: Vec<u16>,
    uniform_buffers: Vec<vk::Buffer>,
    uniform_buffer_memories: Vec<vk::DeviceMemory>,
    start_time: Instant,
    last_window_size: PhysicalSize<u32>,
    descriptor_pool: vk::DescriptorPool,
    descriptor_sets: Arc<Vec<vk::DescriptorSet>>,
    mip_levels: u32,
    depth_image: vk::Image,
    depth_image_memory: vk::DeviceMemory,
    depth_image_view: vk::ImageView,
    depth_format: vk::Format,
    colour_image: vk::Image,
    colour_image_memory: vk::DeviceMemory,
    colour_image_view: vk::ImageView,
    swapchain_recreation_thread: Option<JoinHandle<()>>,
    recreation_request_sender: flume::Sender<(BaseVulkanObjects, SwapchainRecreationRequest)>,
    recreation_request_receiver: Option<flume::Receiver<(BaseVulkanObjects, SwapchainRecreationRequest)>>,
    recreation_response_sender: Option<flume::Sender<SwapchainRecreationResult>>,
    recreation_response_receiver: flume::Receiver<SwapchainRecreationResult>,
    recreating_swapchain: bool,
}

impl Deref for HelloTriangleApplicationData {
    type Target = BaseVulkanObjects;

    fn deref(&self) -> &BaseVulkanObjects {
        &self.base_objects
    }
}

impl DerefMut for HelloTriangleApplicationData {
    fn deref_mut(&mut self) -> &mut BaseVulkanObjects {
        &mut self.base_objects
    }
}

#[derive(Default, Debug, Copy, Clone)]
struct QueueFamilyIndices {
    pub graphics_family: u32,
    pub present_family: u32,
}

impl QueueFamilyIndices {
    pub fn init(physical_device: &vk::PhysicalDevice, surface: &vk::SurfaceKHR) -> Option<Self> {
        let mut graphics_family = None;
        let mut present_family = None;

        for (i, q) in physical_device.get_physical_device_queue_family_properties::<Vec<_>>().iter().enumerate() {
            if q.queue_flags & vk::QueueFlagBits::eGraphicsBit != vk::QueueFlags::eNone && graphics_family.is_none() {
                graphics_family.replace(i as u32);
            }
            if physical_device.get_physical_device_surface_support_khr(i as u32, surface).expect("Unable to query surface support").0.is_true() && present_family.is_none() {
                present_family.replace(i as u32);
            }
        }

        Some(QueueFamilyIndices {
            graphics_family: graphics_family?,
            present_family: present_family?,
        })
    }

    pub fn unique_queues(&self) -> Box<dyn Iterator<Item=u32>> {
        Box::new(std::iter::once(self.graphics_family)
            .chain(std::iter::once(self.present_family))
            .collect::<HashSet<_>>()
            .into_iter())
    }
}

#[derive(Debug)]
struct SwapchainSupportDetails {
    pub capabilities: vk::SurfaceCapabilitiesKHR,
    pub formats: Vec<vk::SurfaceFormatKHR>,
    pub present_modes: Vec<vk::PresentModeKHR>,
}

impl SwapchainSupportDetails {
    pub fn init(physical_device: &vk::PhysicalDevice, surface: &vk::SurfaceKHR) -> Self {
        let capabilities = physical_device.get_physical_device_surface_capabilities_khr(surface).expect("Failed to get device surface capabilities").0;
        let formats = physical_device.get_physical_device_surface_formats_khr::<Vec<_>>(Some(surface)).expect("Failed to get device surface formats").0;
        let present_modes = physical_device.get_physical_device_surface_present_modes_khr::<Vec<_>>(Some(surface)).expect("Failed ot get device surface present modes").0;

        SwapchainSupportDetails {
            capabilities,
            formats,
            present_modes,
        }
    }

    pub fn choose_format(&self) -> vk::SurfaceFormatKHR {
        self.formats.iter().copied().filter(|f| {
            f.format == vk::Format::eB8G8R8A8Srgb && f.colour_space == vk::ColourSpaceKHR::eSrgbNonlinear
        })
            .next().unwrap_or(*self.formats.first().unwrap())
    }

    pub fn choose_present_mode(&self) -> vk::PresentModeKHR {
        let mut mailbox_available = false;
        let mut immediate_available = false;

        for pm in &self.present_modes {
            match *pm {
                vk::PresentModeKHR::eImmediate => immediate_available = true,
                vk::PresentModeKHR::eMailbox => mailbox_available = true,
                _ => {}
            }
        }

        let pm = if mailbox_available {
            vk::PresentModeKHR::eMailbox
        } else if immediate_available {
            vk::PresentModeKHR::eImmediate
        } else {
            vk::PresentModeKHR::eFifo
        };

        println!("Present mode chosen: {:?}", pm);
        pm
    }

    pub fn choose_swap_extent(&self, window_size: PhysicalSize<u32>) -> vk::Extent2D {
        if self.capabilities.current_extent.width != u32::MAX {
            self.capabilities.current_extent
        } else {
            let width = window_size.width.clamp(self.capabilities.min_image_extent.width, self.capabilities.max_image_extent.width);
            let height = window_size.height.clamp(self.capabilities.min_image_extent.height, self.capabilities.max_image_extent.height);

            vk::Extent2D {
                width,
                height,
            }
        }
    }

    pub fn choose_image_count(&self, present_mode: vk::PresentModeKHR) -> u32 {
        let extra = match present_mode {
            vk::PresentModeKHR::eMailbox => 2,
            _ => 1,
        };

        let ic = if self.capabilities.max_image_count == 0 {
            self.capabilities.min_image_count + extra
        } else {
            (self.capabilities.min_image_count + extra).min(self.capabilities.max_image_count)
        };
        println!("Image count choose: {:?}, min count: {:?}, max count: {:?}", ic, self.capabilities.min_image_count, self.capabilities.max_image_count);
        ic
    }
}

enum MainLoopResult {
    OutdatedSwapchain,
    Skipped,
    Success,
}

#[psm::field_align(offset)]
#[derive(Debug, Copy, Clone, PartialEq)]
struct Vertex {
    pub pos: psm::Vector3f,
    pub normal: psm::Vector3f,
    pub tex_coord: psm::Vector2f,
}

impl Vertex {
    pub fn binding_description() -> vk::VertexInputBindingDescription {
        vk::VertexInputBindingDescription {
            binding: 0,
            stride: std::mem::size_of::<Self>() as u32,
            input_rate: vk::VertexInputRate::eVertex,
        }
    }

    pub fn attribute_descriptions() -> [vk::VertexInputAttributeDescription; 3] {
        [
            vk::VertexInputAttributeDescription {
                location: 0,
                binding: 0,
                format: vk::Format::eR32G32B32Sfloat,
                offset: Self::pos_offset() as u32,
            },
            vk::VertexInputAttributeDescription {
                location: 1,
                binding: 0,
                format: vk::Format::eR32G32B32Sfloat,
                offset: Self::normal_offset() as u32,
            },
            vk::VertexInputAttributeDescription {
                location: 2,
                binding: 0,
                format: vk::Format::eR32G32Sfloat,
                offset: Self::tex_coord_offset() as u32,
            }
        ]
    }
}

#[psm::field_align(Self, new, init, offset)]
struct UniformBufferObject {
    #[align(mat4)]
    pub model: psm::Matrix4f,
    #[align(mat4)]
    pub view: psm::Matrix4f,
    #[align(mat4)]
    pub proj: psm::Matrix4f,
}

struct SwapchainRecreationRequest {
    pub command_pool: Arc<RwLock<vk::CommandPool>>,
    pub surface: Arc<RwLock<vk::SurfaceKHR>>,
    pub window_size: PhysicalSize<u32>,
    pub old_swapchain_format: vk::Format,
    pub old_depth_format: vk::Format,
    pub old_renderpass: Arc<vk::RenderPass>,
    pub old_swapchain_image_count: usize,
    pub old_descriptor_sets: Arc<Vec<vk::DescriptorSet>>,
    pub old_swapchain: Arc<RwLock<vk::SwapchainKHR>>,
    pub old_pipeline_layout: Arc<vk::PipelineLayout>,
    pub old_graphics_pipeline: Arc<vk::Pipeline>
}

struct SwapchainRecreationResult {
    pub swapchain: vk::SwapchainKHR,
    pub swapchain_image_format: vk::Format,
    pub swapchain_extent: vk::Extent2D,
    pub swapchain_images: Vec<vk::Image>,
    pub swapchain_image_views: Vec<vk::ImageView>,
    pub render_pass: Arc<vk::RenderPass>,
    pub pipeline_layout: Option<Arc<vk::PipelineLayout>>,
    pub graphics_pipeline: Option<Arc<vk::Pipeline>>,
    pub colour_image: vk::Image,
    pub colour_image_memory: vk::DeviceMemory,
    pub colour_image_view: vk::ImageView,
    pub depth_image: vk::Image,
    pub depth_image_memory: vk::DeviceMemory,
    pub depth_image_view: vk::ImageView,
    pub depth_format: vk::Format,
    pub swapchain_framebuffers: Vec<vk::Framebuffer>,
    pub uniform_buffers: Option<Vec<vk::Buffer>>,
    pub uniform_buffer_memories: Option<Vec<vk::DeviceMemory>>,
    pub descriptor_pool: Option<vk::DescriptorPool>,
    pub descriptor_sets: Option<Vec<vk::DescriptorSet>>,
    pub command_buffers: Vec<vk::CommandBuffer>,
    pub image_available_semaphores: Option<[vk::Semaphore; HelloTriangleApplicationData::MAX_FRAMES_IN_FLIGHT]>,
    pub render_finished_semaphores: Option<[vk::Semaphore; HelloTriangleApplicationData::MAX_FRAMES_IN_FLIGHT]>,
    pub in_flight_frames: Option<[vk::Fence; HelloTriangleApplicationData::MAX_FRAMES_IN_FLIGHT]>,
    pub images_in_flight: Option<Vec<Option<vk::Fence>>>,
}

impl HelloTriangleApplicationData {
    #[cfg(debug_assertions)]
    const VALIDATION_LAYERS: &'static [vk::UnsizedCStr<'static>] = &[vk::unsized_cstr!("VK_LAYER_KHRONOS_validation")];
    #[cfg(not(debug_assertions))]
    const VALIDATION_LAYERS: &'static [vk::UnsizedCStr<'static>] = &[];
    #[cfg(debug_assertions)]
    const DEBUG_EXTENSIONS: &'static [vk::UnsizedCStr<'static>] = &[vk::EXT_DEBUG_UTILS_EXTENSION_NAME_UNSIZED_CSTR];
    #[cfg(not(debug_assertions))]
    const DEBUG_EXTENSIONS: &'static [vk::UnsizedCStr<'static>] = &[];

    const MAX_FRAMES_IN_FLIGHT: usize = 3;
    const MAX_FRAME_TIMES: usize = 100;

    pub fn init_vulkan(&mut self, window: &Window, window_size: PhysicalSize<u32>) {
        self.create_instance(window);

        #[cfg(debug_assertions)]
            self.setup_debug_messenger();

        self.create_surface(window);

        self.pick_physical_device();
        self.create_logical_device();

        let (swapchain, swapchain_image_format, swapchain_extent, swapchain_images) = Self::create_swapchain(&self.base_objects, self.surface.write().deref_mut(), window_size, None);
        self.swapchain = Arc::new(RwLock::new(swapchain));
        self.swapchain_image_format = swapchain_image_format;
        self.swapchain_extent = swapchain_extent;
        self.swapchain_images = swapchain_images;

        self.swapchain_image_views = Self::create_image_views(&self.base_objects, &self.swapchain_images, self.swapchain_image_format);

        self.depth_format = Self::select_depth_format(&self.base_objects);

        self.render_pass = Arc::new(Self::create_render_pass(&self.base_objects, self.swapchain_image_format, self.depth_format));

        self.create_descriptor_set_layout();

        let (pipeline_layout, graphics_pipeline) = Self::create_graphics_pipeline(&self.base_objects, self.swapchain_extent, &self.render_pass);
        self.pipeline_layout = Arc::new(pipeline_layout);
        self.graphics_pipeline = Arc::new(graphics_pipeline);

        self.command_pool = Arc::new(RwLock::new(Self::create_command_pool(&self.base_objects, &self.queue_family_indices)));

        let recreation_request_receiver = self.recreation_request_receiver.take().unwrap();
        let recreation_response_sender = self.recreation_response_sender.take().unwrap();

        self.swapchain_recreation_thread = Some(spawn(move || {
            let recreation_request_receiver = recreation_request_receiver;
            let recreation_response_sender = recreation_response_sender;

            while let Ok((mut base, request)) = recreation_request_receiver.recv() {
                assert!(request.window_size.height != 0 && request.window_size.width != 0);

                // base.device.device_wait_idle().expect("Failed to wait on device idle");

                let (swapchain, swapchain_image_format, swapchain_extent, swapchain_images) = Self::create_swapchain(&base, request.surface.write().deref_mut(), request.window_size, Some(request.old_swapchain.write().deref_mut()));

                let swapchain_image_views = Self::create_image_views(&base, &swapchain_images, swapchain_image_format);

                let depth_format = Self::select_depth_format(&base);

                let (render_pass, pipeline_layout, graphics_pipeline) = if request.old_swapchain_format != swapchain_image_format || request.old_depth_format != depth_format {
                    let render_pass = Self::create_render_pass(&base, swapchain_image_format, depth_format);

                    let (pipeline_layout, graphics_pipeline) = Self::create_graphics_pipeline(&base, swapchain_extent, &render_pass);

                    (Arc::new(render_pass), Arc::new(pipeline_layout), Arc::new(graphics_pipeline))
                } else {
                    (request.old_renderpass.clone(), request.old_pipeline_layout.clone(), request.old_graphics_pipeline.clone())
                };

                let (colour_image, colour_image_memory, colour_image_view) = Self::create_colour_resources(&base, swapchain_extent, swapchain_image_format);

                let (depth_image, depth_image_memory, depth_image_view) = Self::create_depth_resources(&mut base, request.command_pool.write().deref_mut(), swapchain_extent, depth_format);

                let swapchain_framebuffers = Self::create_framebuffers(&base, &render_pass, swapchain_extent, &swapchain_image_views, &colour_image_view, &depth_image_view);

                let (uniform_buffers, uniform_buffer_memories, descriptor_pool, descriptor_sets, image_available_semaphores,render_finished_semaphores,in_flight_frames,images_in_flight) = if request.old_swapchain_image_count != swapchain_images.len() {
                    let (uniform_buffers, uniform_buffer_memories) = Self::create_uniform_buffers(&base, &swapchain_images);

                    let mut descriptor_pool = Self::create_descriptor_pool(&base, &swapchain_images);

                    let descriptor_sets = Self::create_descriptor_sets(&base, &mut descriptor_pool, &base.descriptor_set_layout, &swapchain_images, &uniform_buffers);


                    let (image_available_semaphores,render_finished_semaphores,in_flight_frames,images_in_flight) = Self::create_sync_objects(&base, &swapchain_images);

                    (Some(uniform_buffers), Some(uniform_buffer_memories), Some(descriptor_pool), Some(descriptor_sets), Some(image_available_semaphores), Some(render_finished_semaphores), Some(in_flight_frames), Some(images_in_flight))
                } else {
                    (None, None, None, None, None, None, None, None)
                };

                // command_pool.free_command_buffers(&mut command_buffers);
                let command_buffers = Self::create_command_buffers(&base, request.command_pool.write().deref_mut(), &swapchain_framebuffers, &render_pass, swapchain_extent, &base.vertex_buffer, &base.index_buffer, base.index_size, descriptor_sets.as_ref().unwrap_or(&request.old_descriptor_sets), &pipeline_layout, &graphics_pipeline);


                let result = SwapchainRecreationResult {
                    swapchain,
                    swapchain_image_format,
                    swapchain_extent,
                    swapchain_images,
                    swapchain_image_views,
                    render_pass,
                    pipeline_layout: Some(pipeline_layout),
                    graphics_pipeline: Some(graphics_pipeline),
                    colour_image,
                    colour_image_memory,
                    colour_image_view,
                    depth_image,
                    depth_image_memory,
                    depth_image_view,
                    depth_format,
                    swapchain_framebuffers,
                    uniform_buffers,
                    uniform_buffer_memories,
                    descriptor_pool,
                    descriptor_sets,
                    command_buffers,
                    image_available_semaphores,
                    render_finished_semaphores,
                    in_flight_frames,
                    images_in_flight
                };

                recreation_response_sender.send(result).unwrap();
            }

            ()
        }));

        let (colour_image, colour_image_memory, colour_image_view) = Self::create_colour_resources(&self.base_objects, self.swapchain_extent, self.swapchain_image_format);
        self.colour_image = colour_image;
        self.colour_image_memory = colour_image_memory;
        self.colour_image_view = colour_image_view;

        let (depth_image, depth_image_memory, depth_image_view) = Self::create_depth_resources(&mut self.base_objects, self.command_pool.write().deref_mut(), self.swapchain_extent, self.depth_format);
        self.depth_image = depth_image;
        self.depth_image_memory = depth_image_memory;
        self.depth_image_view = depth_image_view;

        self.swapchain_framebuffers = Self::create_framebuffers(&self.base_objects, &self.render_pass, self.swapchain_extent, &self.swapchain_image_views, &self.colour_image_view, &self.depth_image_view);

        self.create_texture_image();
        self.create_texture_image_views();
        self.create_texture_sampler();

        self.create_vertex_buffer();
        self.create_index_buffer();

        let (uniform_buffers, uniform_buffer_memories) = Self::create_uniform_buffers(&self.base_objects, &self.swapchain_images);
        self.uniform_buffers = uniform_buffers;
        self.uniform_buffer_memories = uniform_buffer_memories;

        self.descriptor_pool = Self::create_descriptor_pool(&self.base_objects, &self.swapchain_images);

        self.descriptor_sets = Arc::new(Self::create_descriptor_sets(&self.base_objects, &mut self.descriptor_pool, &self.base_objects.descriptor_set_layout, &self.swapchain_images, &self.uniform_buffers));

        self.command_pool.write().free_command_buffers(self.command_buffers.drain(0..));
        self.command_buffers = Self::create_command_buffers(&self.base_objects, self.command_pool.write().deref_mut(), &self.swapchain_framebuffers, &self.render_pass, self.swapchain_extent, &self.vertex_buffer, &self.index_buffer, self.indices.len(), &self.descriptor_sets, &self.pipeline_layout, &self.graphics_pipeline);

        let (image_available_semaphores,render_finished_semaphores,in_flight_frames,images_in_flight) = Self::create_sync_objects(&self.base_objects, &self.swapchain_images);
        self.image_available_semaphores = image_available_semaphores;
        self.render_finished_semaphores = render_finished_semaphores;
        self.in_flight_frames = in_flight_frames.map(|f| Arc::new(RwLock::new(f)));
        self.images_in_flight = images_in_flight.into_iter().map(|f| f.map(|f| Arc::new(RwLock::new(f)))).collect();
    }

    pub fn new() -> Self {
        let loader = vk::Loader::new_auto().expect("Failed to load vulkan library");

        let (vertices, indices) = {
            let mut positions = Vec::<psm::Vector3f>::new();
            let mut tex_coords = Vec::<psm::Vector2f>::new();
            let mut normals = Vec::<psm::Vector3f>::new();
            let mut faces = Vec::<(psm::Vector3usize, psm::Vector3usize, psm::Vector3usize)>::new();

            {
                let f = std::fs::read_to_string("res/models/viking_room.obj").expect("Failed to open model file");
                for line in f.lines() {
                    if !line.is_empty() {
                        let mut w = line.split_whitespace();

                        match w.next().unwrap() {
                            "v" => {
                                positions.push(psm::Vector3f::new(
                                    w.next().unwrap().parse().unwrap(),
                                    w.next().unwrap().parse().unwrap(),
                                    w.next().unwrap().parse().unwrap(),
                                ));
                            },
                            "vt" => {
                                tex_coords.push(psm::Vector2f::new(
                                    w.next().unwrap().parse::<f32>().unwrap(),
                                    1.0 - w.next().unwrap().parse::<f32>().unwrap(),
                                ));
                            },
                            "vn" => {
                                normals.push(psm::Vector3f::new(
                                    w.next().unwrap().parse().unwrap(),
                                    w.next().unwrap().parse().unwrap(),
                                    w.next().unwrap().parse().unwrap(),
                                ));
                            },
                            "f" => {
                                faces.push((
                                    {
                                        let mut indices = w.next().unwrap().split('/');
                                        Vector3usize::new(
                                            indices.next().unwrap().parse::<usize>().unwrap() - 1usize,
                                            indices.next().unwrap().parse::<usize>().unwrap() - 1usize,
                                            indices.next().unwrap().parse::<usize>().unwrap() - 1usize,
                                        )
                                    },
                                    {
                                        let mut indices = w.next().unwrap().split('/');
                                        Vector3usize::new(
                                            indices.next().unwrap().parse::<usize>().unwrap() - 1usize,
                                            indices.next().unwrap().parse::<usize>().unwrap() - 1usize,
                                            indices.next().unwrap().parse::<usize>().unwrap() - 1usize,
                                        )
                                    },
                                    {
                                        let mut indices = w.next().unwrap().split('/');
                                        Vector3usize::new(
                                            indices.next().unwrap().parse::<usize>().unwrap() - 1usize,
                                            indices.next().unwrap().parse::<usize>().unwrap() - 1usize,
                                            indices.next().unwrap().parse::<usize>().unwrap() - 1usize,
                                        )
                                    }
                                ));
                            }
                            _ => {}
                        }
                    }
                }
            }

            let mut vertices = Vec::new();
            let mut indices = Vec::new();

            let mut vertex_location = HashMap::<(usize, usize, usize), usize>::new();

            for (i, j, k) in faces {
                for p in [i, j, k] {
                    let v = p[0];
                    let t = p[1];
                    let n = p[2];

                    let vert = Vertex {
                        pos: psm::Vector3f::new(positions[v].x, positions[v].y, positions[v].z),
                        normal: psm::Vector3f::new(normals[n].x, normals[n].y, normals[n].z),
                        tex_coord: psm::Vector2f::new(tex_coords[t].s, tex_coords[t].t),
                    };

                    let index = *vertex_location.entry((v, t, n)).or_insert_with(|| {
                        vertices.push(vert);
                        vertices.len() - 1
                    });

                    indices.push(index as u16);
                }
            }

            dbg!(vertices.len());

            (vertices, indices)
        };

        let (recreation_request_sender, recreation_request_receiver) = flume::bounded::<(BaseVulkanObjects, SwapchainRecreationRequest)>(0);
        let (recreation_response_sender, recreation_response_receiver) = flume::bounded::<SwapchainRecreationResult>(0);

        HelloTriangleApplicationData {
            base_objects: BaseVulkanObjects {
                loader: Arc::new(loader),
                instance: Arc::new(vk::Handle::null()),
                physical_device: Arc::new(vk::Handle::null()),
                queue_family_indices: Default::default(),
                device: Arc::new(RwLock::new(vk::Handle::null())),
                graphics_queue: Arc::new(RwLock::new(vk::Handle::null())),
                present_queue: Arc::new(RwLock::new(vk::Handle::null())),
                msaa_samples: vk::SampleCountFlagBits::e1Bit,
                descriptor_set_layout: Arc::new(vk::Handle::null()),
                texture_image: Arc::new(vk::Handle::null()),
                texture_image_memory: Arc::new(vk::Handle::null()),
                texture_image_view: Arc::new(vk::Handle::null()),
                texture_sampler: Arc::new(vk::Handle::null()),
                vertex_buffer: Arc::new(vk::Handle::null()),
                vertex_buffer_memory: Arc::new(vk::Handle::null()),
                index_buffer: Arc::new(vk::Handle::null()),
                index_buffer_memory: Arc::new(vk::Handle::null()),
                index_size: indices.len()
            },
            surface: Arc::new(RwLock::new(vk::Handle::null())),
            debug_messenger: None,
            swapchain: Arc::new(RwLock::new(vk::Handle::null())),
            swapchain_image_format: vk::Format::eUndefined,
            swapchain_extent: vk::Extent2D { width: 0, height: 0 },
            swapchain_images: vec![],
            swapchain_image_views: vec![],
            render_pass: Arc::new(vk::Handle::null()),
            pipeline_layout: Arc::new(vk::Handle::null()),
            graphics_pipeline: Arc::new(vk::Handle::null()),
            swapchain_framebuffers: vec![],
            command_pool: Arc::new(RwLock::new(vk::Handle::null())),
            command_buffers: vec![],
            image_available_semaphores: array_init::array_init(|_| vk::Handle::null()),
            render_finished_semaphores: array_init::array_init(|_| vk::Handle::null()),
            in_flight_frames: array_init::array_init(|_| Arc::new(RwLock::new(vk::Handle::null()))),
            images_in_flight: vec![],
            current_frame: 0,
            acquired: None,
            present_times: VecDeque::with_capacity(Self::MAX_FRAME_TIMES),
            vertices,
            indices,
            uniform_buffers: vec![],
            uniform_buffer_memories: vec![],
            start_time: Instant::now(),
            last_window_size: Default::default(),
            descriptor_pool: vk::Handle::null(),
            descriptor_sets: Arc::new(vec![]),
            mip_levels: 0,
            depth_image: vk::Handle::null(),
            depth_image_memory: vk::Handle::null(),
            depth_image_view: vk::Handle::null(),
            depth_format: vk::Format::eUndefined,
            colour_image: vk::Handle::null(),
            colour_image_memory: vk::Handle::null(),
            colour_image_view: vk::Handle::null(),
            swapchain_recreation_thread: None,
            recreation_request_sender,
            recreation_request_receiver: Some(recreation_request_receiver),
            recreation_response_sender: Some(recreation_response_sender),
            recreation_response_receiver,
            recreating_swapchain: false
        }
    }

    pub fn main_loop(&mut self) -> MainLoopResult {
        self.draw_frame()
    }

    pub fn clean_up(&mut self) {
        self.device.write().device_wait_idle().expect("Failed to wait for device idle");
    }

    fn create_instance(&mut self, window: &Window) {
        const APP_NAME: vk::UnsizedCStr = vk::unsized_cstr!("Hello Triangle");
        const ENGINE_NAME: vk::UnsizedCStr = vk::unsized_cstr!("No Engine");

        let app_info = vk::ApplicationInfo {
            application_name: Some(APP_NAME),
            application_version: vk::Version::new(0, 1, 0, 0),
            engine_name: Some(ENGINE_NAME),
            engine_version: vk::Version::new(0, 1, 0, 0),
            api_version: vk::API_VERSION_1_2,
        }.into_raw();

        let required_extensions = Self::list_required_instance_extensions(window);

        let available_extensions = self.loader.enumerate_instance_extension_properties::<Vec<_>>(None).expect("Failed to enumerate instance extensions").0;
        let unique_available_extensions = available_extensions.iter()
            .map(|e| e.extension_name.as_cstr().unwrap()).collect::<HashSet<_>>();

        let extensions_supported = required_extensions.iter().all(|e| unique_available_extensions.contains(e.as_cstr()));
        assert!(extensions_supported, "Not all extensions are supported");

        assert!(self.check_validation_layer_support(Self::VALIDATION_LAYERS), "Validation layers requested, but not available.");

        #[cfg(debug_assertions)]
            let mut debug_messenger = Self::debug_messenger_create_info().into_raw();
        #[cfg(debug_assertions)]
            let p_next = [&mut debug_messenger as &mut dyn vk::InstanceCreateInfoNext];

        let create_info = vk::InstanceCreateInfoWithNext {
            #[cfg(debug_assertions)]
            p_next,
            #[cfg(not(debug_assertions))]
            p_next: [],
            flags: Default::default(),
            application_info: Some(&app_info),
            enabled_layer_names: Self::VALIDATION_LAYERS.into(),
            enabled_extension_names: &required_extensions[..],
        }.into_raw();

        self.instance = Arc::new(self.loader.create_instance(&create_info).expect("Failed to create instance").0)
    }

    fn setup_debug_messenger(&mut self) {
        let create_info = Self::debug_messenger_create_info().into_raw();

        self.debug_messenger = Some(self.instance.create_debug_utils_messenger_ext(&create_info).expect("Failed to create debug utils messenger").0)
    }

    fn create_surface(&mut self, window: &Window) {
        self.surface = Arc::new(RwLock::new(self.instance.create_surface(window).expect("Platform not supported").expect("Failed to create surface").0));
    }

    fn pick_physical_device(&mut self) {
        let check_device_extension_support = |physical_device: &vk::PhysicalDevice| -> bool {
            let extensions = physical_device.enumerate_device_extension_properties::<Vec<_>>(None).expect("Failed to enumerate device extensions").0;
            let unique_extensions = extensions.iter()
                .map(|e| e.extension_name.as_cstr().unwrap())
                .collect::<HashSet<_>>();

            self.list_required_device_extensions().iter().all(|e| unique_extensions.contains(e.as_cstr()))
        };

        let is_device_suitable = |physical_device: &vk::PhysicalDevice| -> bool {
            let properties = physical_device.get_physical_device_properties();
            let features = physical_device.get_physical_device_features();

            let queues = QueueFamilyIndices::init(physical_device, self.surface.read().deref());

            let swapchain_support = SwapchainSupportDetails::init(physical_device, self.surface.read().deref());

            properties.device_type == vk::PhysicalDeviceType::eDiscreteGpu
                && features.sampler_anisotropy
                && queues.is_some()
                && !swapchain_support.formats.is_empty()
                && !swapchain_support.present_modes.is_empty()
        };

        let physical_device = self.instance.enumerate_physical_devices::<Vec<_>>().expect("Failed to enumerate physical devices").0.into_iter()
            .filter(check_device_extension_support)
            .filter(is_device_suitable)
            .next();

        self.physical_device = Arc::new(physical_device.expect("Failed to find suitable physical device"));
        #[cfg(debug_assertions)]
        println!("Picked device: {}", self.physical_device.get_physical_device_properties().device_name.to_string().unwrap());

        self.base_objects.queue_family_indices = QueueFamilyIndices::init(&self.base_objects.physical_device, self.surface.read().deref()).unwrap();
        #[cfg(debug_assertions)]
        println!("Picked queue families: {:?}", self.queue_family_indices);

        {
            let physical_device_properties = self.physical_device.get_physical_device_properties();

            let counts = physical_device_properties.limits.framebuffer_colour_sample_counts & physical_device_properties.limits.framebuffer_depth_sample_counts;
            let options = [vk::SampleCountFlagBits::e64Bit, vk::SampleCountFlagBits::e32Bit, vk::SampleCountFlagBits::e16Bit, vk::SampleCountFlagBits::e8Bit,
                                                vk::SampleCountFlagBits::e4Bit, vk::SampleCountFlagBits::e2Bit, vk::SampleCountFlagBits::e1Bit];

            self.msaa_samples = IntoIterator::into_iter(options).find(|s| counts & *s == (*s).into()).unwrap();
            println!("Picked MSAA samples: {:?}", self.msaa_samples);
        }
    }

    fn create_logical_device(&mut self) {
        let priority = [1.0];

        let queue_create_infos = self.queue_family_indices.unique_queues().map(|q| {
            vk::DeviceQueueCreateInfo {
                flags: Default::default(),
                queue_family_index: q,
                queue_priorities: &priority[..],
            }.into_raw()
        }).collect::<Vec<_>>();

        let extensions = self.list_required_device_extensions();

        let mut device_features = vk::PhysicalDeviceFeatures::all_false();
        device_features.sampler_anisotropy = true;
        device_features.sample_rate_shading = true;
        let device_features = device_features.into_raw();

        let create_info = vk::DeviceCreateInfo {
            flags: Default::default(),
            queue_create_infos: &queue_create_infos[..],
            enabled_layer_names: Self::VALIDATION_LAYERS.into(),
            enabled_extension_names: &extensions[..],
            enabled_features: Some(&device_features),
        }.into_raw();

        self.device = Arc::new(RwLock::new(self.physical_device.create_device(&create_info).expect("Failed to create device").0));

        self.base_objects.graphics_queue = Arc::new(RwLock::new(self.base_objects.device.read().get_device_queue(self.base_objects.queue_family_indices.graphics_family, 0)));
        self.base_objects.present_queue = if self.base_objects.queue_family_indices.graphics_family == self.base_objects.queue_family_indices.present_family {
            self.base_objects.graphics_queue.clone()
        } else {
            Arc::new(RwLock::new(self.base_objects.device.read().get_device_queue(self.base_objects.queue_family_indices.present_family, 0)))
        };
    }

    fn create_swapchain(vk: &BaseVulkanObjects, surface: &mut vk::SurfaceKHR, window_size: PhysicalSize<u32>, old_swapchain: Option<&mut vk::SwapchainKHR>) -> (vk::SwapchainKHR, vk::Format, vk::Extent2D, Vec<vk::Image>){
        let swapchain_support = SwapchainSupportDetails::init(&vk.physical_device, surface);

        let surface_format = swapchain_support.choose_format();
        let present_mode = swapchain_support.choose_present_mode();
        let extent = swapchain_support.choose_swap_extent(window_size);
        let image_count = swapchain_support.choose_image_count(present_mode);

        let queue_families = [vk.queue_family_indices.present_family, vk.queue_family_indices.graphics_family];
        let (image_sharing_mode, queue_family_indices) = if vk.queue_family_indices.present_family == vk.queue_family_indices.graphics_family {
            (vk::SharingMode::eExclusive, &[][..])
        } else {
            (vk::SharingMode::eConcurrent, &queue_families[..])
        };

        let mut create_info = vk::SwapchainCreateInfoKHR {
            flags: Default::default(),
            min_image_count: image_count,
            image_format: surface_format.format,
            image_colour_space: surface_format.colour_space,
            image_extent: extent,
            image_array_layers: 1,
            image_usage: vk::ImageUsageFlags::eColourAttachmentBit,
            image_sharing_mode,
            queue_family_indices: queue_family_indices.into(),
            pre_transform: swapchain_support.capabilities.current_transform,
            composite_alpha: vk::CompositeAlphaFlagBitsKHR::eOpaqueBit,
            present_mode,
            clipped: true,
            old_swapchain: old_swapchain.map(|s| s.as_mut_handle()).unwrap_or(vk::Handle::null()),
        }.into_raw();

        // self.swapchain.clone().destroy(); // Needed since the below line would try to create a new one before dropping the old one, and a window can only have 1 surface
        let swapchain = vk.device.read().create_swapchain_khr(surface, &mut create_info).expect("Failed to create swapchain").0;
        let swapchain_image_format = surface_format.format;
        let swapchain_extent = extent;
        let swapchain_images = swapchain.get_swapchain_images_khr::<Vec<_>>(vk.device.read().deref()).expect("Failed to get swapchain images").0;

        (swapchain, swapchain_image_format, swapchain_extent, swapchain_images)
    }

    fn create_image_view(vk: &BaseVulkanObjects, image: &vk::Image, format: vk::Format, aspect_flags: vk::ImageAspectFlags, mip_levels: u32) -> vk::ImageView {
        let create_info = vk::ImageViewCreateInfo {
            flags: Default::default(),
            image: image.as_handle(),
            view_type: vk::ImageViewType::e2d,
            format,
            components: vk::ComponentMapping {
                r: vk::ComponentSwizzle::eIdentity,
                g: vk::ComponentSwizzle::eIdentity,
                b: vk::ComponentSwizzle::eIdentity,
                a: vk::ComponentSwizzle::eIdentity,
            },
            subresource_range: vk::ImageSubresourceRange {
                aspect_mask: aspect_flags,
                base_mip_level: 0,
                level_count: mip_levels,
                base_array_layer: 0,
                layer_count: 1,
            },
        }.into_raw();

        vk.device.read().create_image_view(&create_info).expect("Failed to create image view").0
    }

    fn create_image_views(vk: &BaseVulkanObjects, swapchain_images: &Vec<vk::Image>, swapchain_image_format: vk::Format) -> Vec<vk::ImageView> {
        swapchain_images.iter().map(|i| Self::create_image_view(vk, i, swapchain_image_format, vk::ImageAspectFlags::eColourBit, 1)).collect()
    }

    fn create_render_pass(vk: &BaseVulkanObjects, swapchain_image_format: vk::Format, depth_format: vk::Format) -> vk::RenderPass {
        let attachments = [
            vk::AttachmentDescription {
                flags: Default::default(),
                format: swapchain_image_format,
                samples: vk.msaa_samples,
                load_op: vk::AttachmentLoadOp::eClear,
                store_op: vk::AttachmentStoreOp::eStore,
                stencil_load_op: vk::AttachmentLoadOp::eDontCare,
                stencil_store_op: vk::AttachmentStoreOp::eDontCare,
                initial_layout: vk::ImageLayout::eUndefined,
                final_layout: vk::ImageLayout::eColourAttachmentOptimal,
            }.into_raw(),
            vk::AttachmentDescription {
                flags: Default::default(),
                format: depth_format,
                samples: vk.msaa_samples,
                load_op: vk::AttachmentLoadOp::eClear,
                store_op: vk::AttachmentStoreOp::eDontCare,
                stencil_load_op: vk::AttachmentLoadOp::eDontCare,
                stencil_store_op: vk::AttachmentStoreOp::eDontCare,
                initial_layout: vk::ImageLayout::eUndefined,
                final_layout: vk::ImageLayout::eDepthStencilAttachmentOptimal
            }.into_raw(),
            vk::AttachmentDescription {
                flags: Default::default(),
                format: swapchain_image_format,
                samples: vk::SampleCountFlagBits::e1Bit,
                load_op: vk::AttachmentLoadOp::eDontCare,
                store_op: vk::AttachmentStoreOp::eStore,
                stencil_load_op: vk::AttachmentLoadOp::eDontCare,
                stencil_store_op: vk::AttachmentStoreOp::eDontCare,
                initial_layout: vk::ImageLayout::eUndefined,
                final_layout: vk::ImageLayout::ePresentSrcKhr
            }.into_raw()
        ];

        let colour_attachment_refs = [vk::AttachmentReference {
            attachment: 0,
            layout: vk::ImageLayout::eColourAttachmentOptimal,
        }.into_raw()];

        let depth_attachment_ref = vk::AttachmentReference {
            attachment: 1,
            layout: vk::ImageLayout::eDepthStencilAttachmentOptimal
        }.into_raw();

        let resolve_attachment_refs = [vk::AttachmentReference {
            attachment: 2,
            layout: vk::ImageLayout::eColourAttachmentOptimal
        }.into_raw()];

        let subpasses = [vk::SubpassDescription {
            flags: Default::default(),
            pipeline_bind_point: vk::PipelineBindPoint::eGraphics,
            input_attachments: Default::default(),
            colour_attachments: &colour_attachment_refs[..],
            resolve_attachments: &resolve_attachment_refs[..],
            depth_stencil_attachment: Some(&depth_attachment_ref),
            preserve_attachments: Default::default(),
        }.into_raw()];

        let dependencies = [vk::SubpassDependency {
            src_subpass: vk::SUBPASS_EXTERNAL,
            dst_subpass: 0,
            src_stage_mask: vk::PipelineStageFlags::eColourAttachmentOutputBit | vk::PipelineStageFlags::eEarlyFragmentTestsBit,
            dst_stage_mask: vk::PipelineStageFlags::eColourAttachmentOutputBit | vk::PipelineStageFlags::eEarlyFragmentTestsBit,
            src_access_mask: vk::AccessFlags::eNoneKhr,
            dst_access_mask: vk::AccessFlags::eColourAttachmentWriteBit | vk::AccessFlags::eDepthStencilAttachmentWriteBit,
            dependency_flags: Default::default(),
        }.into_raw()];

        let render_pass_create_info = vk::RenderPassCreateInfo {
            flags: Default::default(),
            attachments: &attachments[..],
            subpasses: &subpasses[..],
            dependencies: &dependencies[..],
        }.into_raw();

        vk.device.read().create_render_pass(&render_pass_create_info).expect("Failed to create render pass").0
    }

    fn create_descriptor_set_layout(&mut self) {
        let ubo_layout_bindings = [
            vk::DescriptorSetLayoutBinding {
                binding: 0,
                descriptor_type: vk::DescriptorType::eUniformBuffer,
                descriptor_count: 1,
                stage_flags: vk::ShaderStageFlags::eVertexBit,
                immutable_samplers: Default::default(),
            }.into_raw(),
            vk::DescriptorSetLayoutBinding {
                binding: 1,
                descriptor_type: vk::DescriptorType::eCombinedImageSampler,
                descriptor_count: 1,
                stage_flags: vk::ShaderStageFlags::eFragmentBit,
                immutable_samplers: &[],
            }.into_raw(),
        ];

        let layout_info = vk::DescriptorSetLayoutCreateInfo {
            flags: Default::default(),
            bindings: &ubo_layout_bindings[..],
        }.into_raw();

        let descriptor_set_layout = Arc::new(self.device.read().create_descriptor_set_layout(&layout_info).expect("Failed to create descriptor set layout").0);
        self.descriptor_set_layout = descriptor_set_layout;
    }

    fn create_graphics_pipeline(vk: &BaseVulkanObjects, swapchain_extent: vk::Extent2D, render_pass: &vk::RenderPass) -> (vk::PipelineLayout, vk::Pipeline) {
        fn load_shader_code<P>(path: P) -> Vec<u32> where P: AsRef<Path> {
            fn data_as_bytes_mut(data: &mut [u32]) -> &mut [u8] {
                let len = data.len() * 4;
                unsafe { std::slice::from_raw_parts_mut(data.as_ptr() as *mut u8, len) }
            }

            let mut f = std::fs::File::open(path)
                .expect("Failed to open shader code file");

            let len = f.seek(SeekFrom::End(0)).unwrap() as usize;
            f.seek(SeekFrom::Start(0)).unwrap();

            assert_eq!(len % 4, 0, "");

            let mut data = vec![0u32; len / 4];

            f.read_exact(data_as_bytes_mut(data.as_mut_slice())).expect("Failed to read data");

            data
        }

        fn create_shader_module(device: &vk::Device, code: &[u32]) -> vk::ShaderModule {
            let create_info = vk::ShaderModuleCreateInfo {
                flags: Default::default(),
                code: code,
            }.into_raw();

            device.create_shader_module(&create_info).expect("Failed to create shader module").0
        }

        let vertex_shader_code = load_shader_code("res/shaders/vert.spv");
        let fragment_shader_code = load_shader_code("res/shaders/frag.spv");

        let vertex_shader_module = create_shader_module(vk.device.read().deref(), &vertex_shader_code[..]);
        let fragment_shader_module = create_shader_module(vk.device.read().deref(), &fragment_shader_code[..]);

        let shader_stages = [
            vk::PipelineShaderStageCreateInfo {
                flags: Default::default(),
                stage: vk::ShaderStageFlagBits::eVertexBit,
                module: vertex_shader_module.as_handle(),
                name: vk::unsized_cstr!("main"),
                specialization_info: None,
            }.into_raw(),
            vk::PipelineShaderStageCreateInfo {
                flags: Default::default(),
                stage: vk::ShaderStageFlagBits::eFragmentBit,
                module: fragment_shader_module.as_handle(),
                name: vk::unsized_cstr!("main"),
                specialization_info: None,
            }.into_raw(),
        ];

        let binding_descriptions = [Vertex::binding_description().into_raw()];
        let attribute_descriptions = Vertex::attribute_descriptions().map(|s| s.into_raw());

        let vertex_input_info = vk::PipelineVertexInputStateCreateInfo {
            flags: Default::default(),
            vertex_binding_descriptions: &binding_descriptions[..],
            vertex_attribute_descriptions: &attribute_descriptions[..],
        }.into_raw();

        let input_assembly = vk::PipelineInputAssemblyStateCreateInfo {
            flags: Default::default(),
            topology: vk::PrimitiveTopology::eTriangleList,
            primitive_restart_enable: false,
        }.into_raw();

        let viewports = [vk::Viewport {
            x: 0.0,
            y: 0.0,
            width: swapchain_extent.width as f32,
            height: swapchain_extent.height as f32,
            min_depth: 0.0,
            max_depth: 1.0,
        }.into_raw()];

        let scissors = [vk::Rect2D {
            offset: vk::Offset2D { x: 0, y: 0 },
            extent: swapchain_extent,
        }.into_raw()];

        let viewport_state = vk::PipelineViewportStateCreateInfo {
            flags: Default::default(),
            viewports: &viewports[..],
            scissors: &scissors[..],
        }.into_raw();

        let rasterizer = vk::PipelineRasterizationStateCreateInfo {
            flags: Default::default(),
            depth_clamp_enable: false,
            rasterizer_discard_enable: false,
            polygon_mode: vk::PolygonMode::eFill,
            cull_mode: vk::CullModeFlags::eBackBit,
            front_face: vk::FrontFace::eCounterClockwise,
            depth_bias_enable: false,
            depth_bias_constant_factor: 0.0,
            depth_bias_clamp: 0.0,
            depth_bias_slope_factor: 0.0,
            line_width: 1.0,
        }.into_raw();

        let multisampling = vk::PipelineMultisampleStateCreateInfo {
            flags: Default::default(),
            rasterization_samples: vk.msaa_samples,
            sample_shading_enable: true,
            min_sample_shading: 1.0,
            sample_mask: &[],
            alpha_to_coverage_enable: false,
            alpha_to_one_enable: false,
        }.into_raw();

        let depth_stencil = vk::PipelineDepthStencilStateCreateInfo {
            flags: Default::default(),
            depth_test_enable: true,
            depth_write_enable: true,
            depth_compare_op: vk::CompareOp::eLess,
            depth_bounds_test_enable: false,
            stencil_test_enable: false,
            front: vk::StencilOpState {
                fail_op: vk::StencilOp::eKeep,
                pass_op: vk::StencilOp::eKeep,
                depth_fail_op: vk::StencilOp::eKeep,
                compare_op: vk::CompareOp::eNever,
                compare_mask: 0,
                write_mask: 0,
                reference: 0
            },
            back: vk::StencilOpState {
                fail_op: vk::StencilOp::eKeep,
                pass_op: vk::StencilOp::eKeep,
                depth_fail_op: vk::StencilOp::eKeep,
                compare_op: vk::CompareOp::eNever,
                compare_mask: 0,
                write_mask: 0,
                reference: 0
            },
            min_depth_bounds: 0.0,
            max_depth_bounds: 1.0
        }.into_raw();

        let colour_blend_attachments = [vk::PipelineColourBlendAttachmentState {
            blend_enable: true,
            src_colour_blend_factor: vk::BlendFactor::eOne,
            dst_colour_blend_factor: vk::BlendFactor::eZero,
            colour_blend_op: vk::BlendOp::eAdd,
            src_alpha_blend_factor: vk::BlendFactor::eOne,
            dst_alpha_blend_factor: vk::BlendFactor::eZero,
            alpha_blend_op: vk::BlendOp::eAdd,
            colour_write_mask: vk::ColourComponentFlags::eRBit | vk::ColourComponentFlags::eGBit | vk::ColourComponentFlags::eBBit | vk::ColourComponentFlags::eABit,
        }.into_raw()];

        let colour_blending = vk::PipelineColourBlendStateCreateInfo {
            flags: Default::default(),
            logic_op_enable: false,
            logic_op: vk::LogicOp::eCopy,
            attachments: &colour_blend_attachments[..],
            blend_constants: [0.0, 0.0, 0.0, 0.0],
        }.into_raw();

        let dynamic_states = [vk::DynamicState::eViewport, vk::DynamicState::eScissor].map(|s| s.into_raw());

        let dynamic_state = vk::PipelineDynamicStateCreateInfo {
            flags: Default::default(),
            dynamic_states: &dynamic_states[..]
        }.into_raw();

        let set_layouts = [vk.descriptor_set_layout.as_handle()];

        let pipeline_layout_creat_info = vk::PipelineLayoutCreateInfo {
            flags: Default::default(),
            set_layouts: &set_layouts[..],
            push_constant_ranges: &[],
        }.into_raw();

        let pipeline_layout = vk.device.read().create_pipeline_layout(&pipeline_layout_creat_info).expect("Failed to create pipeline layout").0;

        let pipeline_create_info = vk::GraphicsPipelineCreateInfo {
            flags: Default::default(),
            stages: &shader_stages[..],
            vertex_input_state: Some(&vertex_input_info),
            input_assembly_state: Some(&input_assembly),
            tessellation_state: None,
            viewport_state: Some(&viewport_state),
            rasterization_state: Some(&rasterizer),
            multisample_state: Some(&multisampling),
            depth_stencil_state: Some(&depth_stencil),
            colour_blend_state: Some(&colour_blending),
            dynamic_state: Some(&dynamic_state),
            layout: pipeline_layout.as_handle(),
            render_pass: render_pass.as_handle(),
            subpass: 0,
            base_pipeline_handle: vk::Handle::null(),
            base_pipeline_index: -1,
        }.into_raw();

        let graphics_pipeline = (vk.device.read().create_graphics_pipelines::<Vec<_>>(None, &[pipeline_create_info]).expect("Failed to create graphics pipeline").0)
            .into_iter().next().unwrap();

        (pipeline_layout, graphics_pipeline)
    }

    fn create_framebuffers(vk: &BaseVulkanObjects, render_pass: &vk::RenderPass, swapchain_extent: vk::Extent2D, swapchain_image_views: &Vec<vk::ImageView>, colour_image_view: &vk::ImageView, depth_image_view: &vk::ImageView) -> Vec<vk::Framebuffer> {
        swapchain_image_views.iter().map(|view| {
            let attachments = [colour_image_view.as_handle(), depth_image_view.as_handle(), view.as_handle()];

            let create_info = vk::FramebufferCreateInfo {
                flags: Default::default(),
                render_pass: render_pass.as_handle(),
                attachments: &attachments[..],
                width: swapchain_extent.width,
                height: swapchain_extent.height,
                layers: 1,
            }.into_raw();

            vk.device.read().create_framebuffer(&create_info).expect("Failed to create framebuffer").0
        }).collect()
    }

    fn create_command_pool(vk: &BaseVulkanObjects, queue_family_indices: &QueueFamilyIndices) -> vk::CommandPool {
        let create_info = vk::CommandPoolCreateInfo {
            flags: Default::default(),
            queue_family_index: queue_family_indices.graphics_family,
        }.into_raw();

        vk.device.read().create_command_pool(&create_info).expect("Failed to create command pool").0
    }

    fn find_memory_type(vk: &BaseVulkanObjects, type_filter: u32, properties: vk::MemoryPropertyFlags) -> u32 {
        let mem_properties = vk.physical_device.get_physical_device_memory_properties();

        // #[cfg(debug_assertions)]
        // dbg!(&mem_properties);

        for i in 0..mem_properties.memory_type_count {
            if type_filter & (1 << i) != 0
                && mem_properties.memory_types[i as usize].property_flags & properties == properties {
                return i;
            }
        }

        panic!("No valid memory type found")
    }

    fn create_buffer(vk: &BaseVulkanObjects, size: vk::DeviceSize, usage: vk::BufferUsageFlags, properties: vk::MemoryPropertyFlags) -> (vk::Buffer, vk::DeviceMemory) {
        let buffer_info = vk::BufferCreateInfo {
            flags: Default::default(),
            size,
            usage,
            sharing_mode: vk::SharingMode::eExclusive,
            queue_family_indices: &[],
        }.into_raw();

        let mut buffer = vk.device.read().create_buffer(&buffer_info).expect("Failed to create buffer").0;

        let memory_requirements = buffer.get_buffer_memory_requirements();

        let alloc_info = vk::MemoryAllocateInfo {
            allocation_size: memory_requirements.size,
            memory_type_index: Self::find_memory_type(vk, memory_requirements.memory_type_bits, properties),
        }.into_raw();

        let device_memory = vk.device.read().allocate_memory(&alloc_info).expect("Failed to allocate buffer").0;

        buffer.bind_buffer_memory(&device_memory, 0).expect("Failed to bind buffer memory");

        (buffer, device_memory)
    }

    fn begin_single_time_command(command_pool: &mut vk::CommandPool) -> vk::CommandBuffer {
        let mut alloc_info = vk::CommandBufferAllocateInfo {
            level: vk::CommandBufferLevel::ePrimary,
            command_buffer_count: 1,
        }.into_raw();

        let mut cmd = command_pool.allocate_command_buffers::<Vec<_>>(&mut alloc_info).expect("Failed to allocate command buffer").0.into_iter().next().unwrap();

        let begin_info = vk::CommandBufferBeginInfo {
            flags: vk::CommandBufferUsageFlags::eOneTimeSubmitBit,
            inheritance_info: None,
        }.into_raw();

        cmd.begin_command_buffer(command_pool, &begin_info).expect("Failed to begin reading command buffer");

        cmd
    }

    fn end_single_time_command(command_pool: &mut vk::CommandPool, queue: &mut vk::Queue, mut cmd: vk::CommandBuffer) {
        cmd.end_command_buffer(command_pool).expect("Failed to end recoding command buffer");

        let command_buffers = [cmd.as_handle()];

        let submit_infos = [vk::SubmitInfo {
            wait_semaphores: &[],
            wait_dst_stage_mask: &[],
            command_buffers: &command_buffers[..],
            signal_semaphores: &[],
        }.into_raw()];

        queue.queue_submit(&submit_infos[..], None).expect("Failed to submit copy to queue");
        queue.queue_wait_idle().expect("Failed to wait on queue idle");

        command_pool.free_command_buffers([cmd]);
    }

    fn copy_buffer(&self, src: &vk::Buffer, dst: &vk::Buffer, size: vk::DeviceSize) {
        let mut cmd = Self::begin_single_time_command(self.command_pool.write().deref_mut());

        let copy_regions = [vk::BufferCopy {
            src_offset: 0,
            dst_offset: 0,
            size,
        }.into_raw()];
        cmd.cmd_copy_buffer(self.command_pool.write().deref_mut(), src, dst, &copy_regions[..]);

        Self::end_single_time_command(self.command_pool.write().deref_mut(), self.graphics_queue.write().deref_mut(), cmd);
    }

    fn create_image(vk: &BaseVulkanObjects, (width, height): (u32, u32), mip_levels: u32, num_samples: vk::SampleCountFlagBits, format: vk::Format, tiling: vk::ImageTiling, usage: vk::ImageUsageFlags, properties: vk::MemoryPropertyFlags) -> (vk::Image, vk::DeviceMemory) {
        let image_info = vk::ImageCreateInfo {
            flags: Default::default(),
            image_type: vk::ImageType::e2d,
            format,
            extent: vk::Extent3D {
                width,
                height,
                depth: 1,
            },
            mip_levels,
            array_layers: 1,
            samples: num_samples,
            tiling,
            usage,
            sharing_mode: vk::SharingMode::eExclusive,
            queue_family_indices: &[],
            initial_layout: vk::ImageLayout::eUndefined,
        }.into_raw();

        let mut texture_image = vk.device.read().create_image(&image_info).expect("Failed to create image").0;

        let memory_requirements = texture_image.get_image_memory_requirements();

        let alloc_info = vk::MemoryAllocateInfo {
            allocation_size: memory_requirements.size,
            memory_type_index: Self::find_memory_type(vk, memory_requirements.memory_type_bits, properties),
        }.into_raw();

        let texture_image_memory = vk.device.read().allocate_memory(&alloc_info).expect("Failed to allocate image memory").0;

        texture_image.bind_image_memory(&texture_image_memory, 0).expect("Failed to bind texture");

        (texture_image, texture_image_memory)
    }

    fn transition_image_layout(vk: &mut BaseVulkanObjects, command_pool: &mut vk::CommandPool, image: &vk::Image, format: vk::Format, old_layout: vk::ImageLayout, new_layout: vk::ImageLayout, mip_levels: u32) {
        let mut cmd = Self::begin_single_time_command(command_pool);

        let aspect_mask = if new_layout == vk::ImageLayout::eDepthStencilAttachmentOptimal {
            if Self::depth_has_stencil(format) {
                vk::ImageAspectFlags::eDepthBit | vk::ImageAspectFlags::eStencilBit
            } else {
                vk::ImageAspectFlags::eDepthBit
            }
        } else {
            vk::ImageAspectFlags::eColourBit
        };

        let (src_access_mask, dst_access_mask, source_stage, destination_stage) = match (old_layout, new_layout) {
            (vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal) => {
                (
                    vk::AccessFlags::eNoneKhr, vk::AccessFlags::eTransferWriteBit,
                    vk::PipelineStageFlags::eTopOfPipeBit, vk::PipelineStageFlags::eTransferBit
                )
            }
            (vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal) => {
                (
                    vk::AccessFlags::eTransferWriteBit, vk::AccessFlags::eShaderReadBit,
                    vk::PipelineStageFlags::eTransferBit, vk::PipelineStageFlags::eFragmentShaderBit
                )
            }
            (vk::ImageLayout::eUndefined, vk::ImageLayout::eDepthStencilAttachmentOptimal) => {
                (
                    vk::AccessFlags::eNoneKhr, vk::AccessFlags::eDepthStencilAttachmentReadBit | vk::AccessFlags::eDepthStencilAttachmentWriteBit,
                    vk::PipelineStageFlags::eTopOfPipeBit, vk::PipelineStageFlags::eEarlyFragmentTestsBit
                )
            }
            (o, n) => panic!("Unsupported layout transition: {:?} -> {:?}", o, n)
        };

        let barriers = [vk::ImageMemoryBarrier {
            src_access_mask,
            dst_access_mask,
            old_layout,
            new_layout,
            src_queue_family_index: vk::QUEUE_FAMILY_IGNORED,
            dst_queue_family_index: vk::QUEUE_FAMILY_IGNORED,
            image: image.as_handle(),
            subresource_range: vk::ImageSubresourceRange {
                aspect_mask,
                base_mip_level: 0,
                level_count: mip_levels,
                base_array_layer: 0,
                layer_count: 1,
            },
        }.into_raw()];

        cmd.cmd_pipeline_barrier(
            command_pool,
            source_stage, destination_stage,
            vk::DependencyFlags::eNone,
            &[],
            &[],
            &barriers[..],
        );

        Self::end_single_time_command(command_pool, vk.graphics_queue.write().deref_mut(), cmd);
    }

    fn copy_image_to_buffer(&mut self, buffer: &vk::Buffer, image: &vk::Image, (width, height): (u32, u32)) {
        let mut cmd = Self::begin_single_time_command(self.command_pool.write().deref_mut());

        let region = vk::BufferImageCopy {
            buffer_offset: 0,
            buffer_row_length: 0,
            buffer_image_height: 0,
            image_subresource: vk::ImageSubresourceLayers {
                aspect_mask: vk::ImageAspectFlags::eColourBit,
                mip_level: 0,
                base_array_layer: 0,
                layer_count: 1,
            },
            image_offset: vk::Offset3D {
                x: 0,
                y: 0,
                z: 0,
            },
            image_extent: vk::Extent3D {
                width,
                height,
                depth: 1,
            },
        }.into_raw();

        cmd.cmd_copy_buffer_to_image(self.command_pool.write().deref_mut(), buffer, image, vk::ImageLayout::eTransferDstOptimal, &[region]);

        Self::end_single_time_command(self.command_pool.write().deref_mut(), self.graphics_queue.write().deref_mut(), cmd);
    }

    fn depth_has_stencil(depth_format: vk::Format) -> bool {
        match depth_format {
            vk::Format::eD24UnormS8Uint | vk::Format::eD32SfloatS8Uint => true,
            _ => false
        }
    }

    fn select_depth_format(vk: &BaseVulkanObjects) -> vk::Format {
        let find_supported_format = |candidates: Vec<vk::Format>, tiling: vk::ImageTiling, features: vk::FormatFeatureFlags| {
            for format in candidates {
                let props = vk.physical_device.get_physical_device_format_properties(format);

                if match tiling {
                    vk::ImageTiling::eOptimal => props.optimal_tiling_features & features == features,
                    vk::ImageTiling::eLinear => props.linear_tiling_features & features == features,
                    _ => panic!("Unsupported tiling: {:?}", tiling),
                } {
                    return format;
                }
            }

            panic!("Failed to find supported format");
        };

        find_supported_format(vec![vk::Format::eD32Sfloat, vk::Format::eD32SfloatS8Uint, vk::Format::eD24UnormS8Uint], vk::ImageTiling::eOptimal, vk::FormatFeatureFlags::eDepthStencilAttachmentBit)
    }

    fn create_colour_resources(vk: &BaseVulkanObjects, swapchain_extent: vk::Extent2D, swapchain_image_format: vk::Format) -> (vk::Image, vk::DeviceMemory, vk::ImageView) {
        let colour_format = swapchain_image_format;

        let (colour_image, colour_image_memory) = Self::create_image(vk, (swapchain_extent.width, swapchain_extent.height), 1, vk.msaa_samples, colour_format, vk::ImageTiling::eOptimal,
                                                                    vk::ImageUsageFlags::eTransientAttachmentBit | vk::ImageUsageFlags::eColourAttachmentBit, vk::MemoryPropertyFlags::eDeviceLocalBit);

        let colour_image_view = Self::create_image_view(vk, &colour_image, colour_format, vk::ImageAspectFlags::eColourBit, 1);

        (colour_image, colour_image_memory, colour_image_view)
    }

    fn create_depth_resources(vk: &mut BaseVulkanObjects, command_pool: &mut vk::CommandPool, swapchain_extent: vk::Extent2D, depth_format: vk::Format) -> (vk::Image, vk::DeviceMemory, vk::ImageView) {
        let (depth_image, depth_image_memory) = Self::create_image(vk, (swapchain_extent.width, swapchain_extent.height), 1, vk.msaa_samples, depth_format, vk::ImageTiling::eOptimal,
                                                                  vk::ImageUsageFlags::eDepthStencilAttachmentBit, vk::MemoryPropertyFlags::eDeviceLocalBit);

        let depth_image_view = Self::create_image_view(vk, &depth_image, depth_format, vk::ImageAspectFlags::eDepthBit, 1);

        Self::transition_image_layout(vk, command_pool, &depth_image, depth_format, vk::ImageLayout::eUndefined, vk::ImageLayout::eDepthStencilAttachmentOptimal, 1);

        (depth_image, depth_image_memory, depth_image_view)
    }

    fn generate_mipmaps(&mut self, image: &vk::Image, image_format: vk::Format, (width, height): (u32, u32), mip_levels: u32) {

        let format_properties = self.physical_device.get_physical_device_format_properties(image_format);
        if format_properties.optimal_tiling_features & vk::FormatFeatureFlags::eSampledImageFilterLinearBit != vk::FormatFeatureFlags::eSampledImageFilterLinearBit {
            panic!("Texture image format: {:?} does not support linear blitting", image_format);
        }

        let mut cmd = Self::begin_single_time_command(self.command_pool.write().deref_mut());

        let mut barrier = vk::ImageMemoryBarrier {
            src_access_mask: Default::default(),
            dst_access_mask: Default::default(),
            old_layout: vk::ImageLayout::eUndefined,
            new_layout: vk::ImageLayout::eUndefined,
            src_queue_family_index: vk::QUEUE_FAMILY_IGNORED,
            dst_queue_family_index: vk::QUEUE_FAMILY_IGNORED,
            image: image.as_handle(),
            subresource_range: vk::ImageSubresourceRange {
                aspect_mask: vk::ImageAspectFlags::eColourBit,
                base_mip_level: 0,
                level_count: 1,
                base_array_layer: 0,
                layer_count: 1
            }
        }.into_raw();

        let mut mip_width = width;
        let mut mip_height = height;

        for i in 1..mip_levels {

            let next_mip_width = (mip_width / 2).max(1);
            let next_mip_height = (mip_height / 2).max(1);

            barrier.subresource_range.base_mip_level = i - 1;
            barrier.old_layout = vk::ImageLayout::eTransferDstOptimal.into_raw();
            barrier.new_layout = vk::ImageLayout::eTransferSrcOptimal.into_raw();
            barrier.src_access_mask = vk::AccessFlags::eTransferWriteBit;
            barrier.dst_access_mask = vk::AccessFlags::eTransferReadBit;

            cmd.cmd_pipeline_barrier(
                self.command_pool.write().deref_mut(),
                vk::PipelineStageFlags::eTransferBit, vk::PipelineStageFlags::eTransferBit,
                vk::DependencyFlags::eNone,
                &[],
                &[],
                &[barrier.try_copy().unwrap()]
            );

            let blit = vk::ImageBlit {
                src_subresource: vk::ImageSubresourceLayers {
                    aspect_mask: vk::ImageAspectFlags::eColourBit,
                    mip_level: i - 1,
                    base_array_layer: 0,
                    layer_count: 1
                },
                src_offsets: [
                    vk::Offset3D { x: 0, y: 0, z: 0 },
                    vk::Offset3D { x: mip_width as i32, y: mip_height as i32, z: 1},
                ],
                dst_subresource: vk::ImageSubresourceLayers {
                    aspect_mask: vk::ImageAspectFlags::eColourBit,
                    mip_level: i,
                    base_array_layer: 0,
                    layer_count: 1
                },
                dst_offsets: [
                    vk::Offset3D { x: 0, y: 0, z: 0 },
                    vk::Offset3D { x: next_mip_width as i32, y: next_mip_height as i32, z: 1},
                ]
            }.into_raw();

            cmd.cmd_blit_image(
                self.command_pool.write().deref_mut(),
                image, vk::ImageLayout::eTransferSrcOptimal,
                image, vk::ImageLayout::eTransferDstOptimal,
                &[blit],
                vk::Filter::eLinear
            );

            barrier.old_layout = vk::ImageLayout::eTransferSrcOptimal.into_raw();
            barrier.new_layout = vk::ImageLayout::eShaderReadOnlyOptimal.into_raw();
            barrier.src_access_mask = vk::AccessFlags::eTransferReadBit;
            barrier.dst_access_mask = vk::AccessFlags::eShaderReadBit;

            cmd.cmd_pipeline_barrier(
                self.command_pool.write().deref_mut(),
                vk::PipelineStageFlags::eTransferBit, vk::PipelineStageFlags::eFragmentShaderBit,
                vk::DependencyFlags::eNone,
                &[],
                &[],
                &[barrier.try_copy().unwrap()]
            );

            mip_width = next_mip_width;
            mip_height = next_mip_height;
        }

        barrier.subresource_range.base_mip_level = mip_levels - 1;
        barrier.old_layout = vk::ImageLayout::eTransferDstOptimal.into_raw();
        barrier.new_layout = vk::ImageLayout::eShaderReadOnlyOptimal.into_raw();
        barrier.src_access_mask = vk::AccessFlags::eTransferWriteBit;
        barrier.dst_access_mask = vk::AccessFlags::eShaderReadBit;

        cmd.cmd_pipeline_barrier(
            self.command_pool.write().deref_mut(),
            vk::PipelineStageFlags::eTransferBit, vk::PipelineStageFlags::eFragmentShaderBit,
            vk::DependencyFlags::eNone,
            &[],
            &[],
            &[barrier.try_copy().unwrap()]
        );

        Self::end_single_time_command(self.command_pool.write().deref_mut(), self.graphics_queue.write().deref_mut(), cmd);
    }

    fn create_texture_image(&mut self) {
        let image = image::open("res/textures/viking_room.png").expect("Failed to load image").into_rgba8();

        let image_size = (image.width() * image.height() * 4) as vk::DeviceSize;
        self.mip_levels = (image.width().max(image.height()) as f64).log2().floor() as u32 + 1;

        let (staging_buffer, mut staging_buffer_memory) = Self::create_buffer(&self.base_objects, image_size, vk::BufferUsageFlags::eTransferSrcBit, vk::MemoryPropertyFlags::eHostVisibleBit | vk::MemoryPropertyFlags::eHostCoherentBit);

        let mut data = staging_buffer_memory.map_memory(0, image_size, vk::MemoryMapFlags::eNone).expect("Failed to map image staging buffer memory").0;
        data.copy_from_slice(image.as_bytes());
        staging_buffer_memory.unmap_memory(data);

        let (texture_image, texture_image_memory) = Self::create_image(&self.base_objects, image.dimensions(), self.mip_levels,
                                                                      vk::SampleCountFlagBits::e1Bit, vk::Format::eR8G8B8A8Srgb, vk::ImageTiling::eOptimal,
                                                                      vk::ImageUsageFlags::eTransferDstBit | vk::ImageUsageFlags::eTransferSrcBit | vk::ImageUsageFlags::eSampledBit, vk::MemoryPropertyFlags::eDeviceLocalBit);

        Self::transition_image_layout(&mut self.base_objects, self.command_pool.write().deref_mut(), &texture_image, vk::Format::eR8G8B8A8Srgb, vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal, self.mip_levels);
        self.copy_image_to_buffer(&staging_buffer, &texture_image, image.dimensions());
        // self.transition_image_layout(&texture_image, vk::Format::eR8G8B8A8Srgb, vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal, self.mip_levels);
        self.generate_mipmaps(&texture_image, vk::Format::eR8G8B8A8Srgb, image.dimensions(), self.mip_levels);

        self.texture_image = Arc::new(texture_image);
        self.texture_image_memory = Arc::new(texture_image_memory);
    }

    fn create_texture_image_views(&mut self) {
        self.texture_image_view = Arc::new(Self::create_image_view(&self.base_objects, &self.texture_image, vk::Format::eR8G8B8A8Srgb, vk::ImageAspectFlags::eColourBit, self.mip_levels))
    }

    fn create_texture_sampler(&mut self) {
        let properties = self.physical_device.get_physical_device_properties();

        let sampler_info = vk::SamplerCreateInfo {
            flags: Default::default(),
            mag_filter: vk::Filter::eLinear,
            min_filter: vk::Filter::eLinear,
            mipmap_mode: vk::SamplerMipmapMode::eLinear,
            address_mode_u: vk::SamplerAddressMode::eRepeat,
            address_mode_v: vk::SamplerAddressMode::eRepeat,
            address_mode_w: vk::SamplerAddressMode::eRepeat,
            mip_lod_bias: 0.0,
            anisotropy_enable: false,
            max_anisotropy: properties.limits.max_sampler_anisotropy,
            compare_enable: false,
            compare_op: vk::CompareOp::eAlways,
            min_lod: 0.0,
            max_lod: self.mip_levels as f32,
            border_colour: vk::BorderColour::eIntOpaqueBlack,
            unnormalised_coordinates: false,
        }.into_raw();

        let texture_sampler = Arc::new(self.device.read().create_sampler(&sampler_info).expect("Failed to create texture sampler").0);
        self.texture_sampler = texture_sampler;
    }

    fn create_vertex_buffer(&mut self) {
        let device_size = (core::mem::size_of::<Vertex>() * self.vertices.len()) as vk::DeviceSize;

        let (staging_buffer, mut staging_buffer_memory) = Self::create_buffer(&self.base_objects, device_size, vk::BufferUsageFlags::eTransferSrcBit, vk::MemoryPropertyFlags::eHostVisibleBit | vk::MemoryPropertyFlags::eHostCoherentBit);

        let mut data = staging_buffer_memory.map_memory(0, device_size, vk::MemoryMapFlags::eNone).expect("Failed to map staging vertex memory").0;
        let vertices = unsafe { std::slice::from_raw_parts(self.vertices.as_ptr() as *const u8, self.vertices.len() * std::mem::size_of::<Vertex>()) };
        data.copy_from_slice(vertices);
        staging_buffer_memory.unmap_memory(data);

        let (vertex_buffer, vertex_buffer_memory) = Self::create_buffer(&self.base_objects, device_size, vk::BufferUsageFlags::eVertexBufferBit | vk::BufferUsageFlags::eTransferDstBit, vk::MemoryPropertyFlags::eHostVisibleBit | vk::MemoryPropertyFlags::eHostCoherentBit);
        self.vertex_buffer = Arc::new(vertex_buffer);
        self.vertex_buffer_memory = Arc::new(vertex_buffer_memory);

        self.copy_buffer(&staging_buffer, &self.vertex_buffer, device_size);
    }

    fn create_index_buffer(&mut self) {
        let device_size = (core::mem::size_of::<u16>() * self.indices.len()) as vk::DeviceSize;

        let (staging_buffer, mut staging_buffer_memory) = Self::create_buffer(&self.base_objects, device_size, vk::BufferUsageFlags::eTransferSrcBit, vk::MemoryPropertyFlags::eHostVisibleBit | vk::MemoryPropertyFlags::eHostCoherentBit);

        let mut data = staging_buffer_memory.map_memory(0, device_size, vk::MemoryMapFlags::eNone).expect("Failed to map staging index memory").0;
        let indices = unsafe { std::slice::from_raw_parts(self.indices.as_ptr() as *const u8, self.indices.len() * std::mem::size_of::<u16>()) };
        data.copy_from_slice(indices);
        staging_buffer_memory.unmap_memory(data);

        let (index_buffer, index_buffer_memory) = Self::create_buffer(&self.base_objects, device_size, vk::BufferUsageFlags::eIndexBufferBit | vk::BufferUsageFlags::eTransferDstBit, vk::MemoryPropertyFlags::eHostVisibleBit | vk::MemoryPropertyFlags::eHostCoherentBit);
        self.index_buffer = Arc::new(index_buffer);
        self.index_buffer_memory = Arc::new(index_buffer_memory);

        self.copy_buffer(&staging_buffer, &self.index_buffer, device_size);
    }

    fn create_uniform_buffers(vk: &BaseVulkanObjects, swapchain_images: &Vec<vk::Image>) -> (Vec<vk::Buffer>, Vec<vk::DeviceMemory>){
        let device_size = core::mem::size_of::<UniformBufferObject>() as vk::DeviceSize;

        let mut uniform_buffers = Vec::with_capacity(swapchain_images.len());
        let mut uniform_buffer_memories = Vec::with_capacity(swapchain_images.len());

        for _ in 0..swapchain_images.len() {
            let (uniform_buffer, uniform_buffer_memory) = Self::create_buffer(vk, device_size, vk::BufferUsageFlags::eUniformBufferBit, vk::MemoryPropertyFlags::eHostVisibleBit | vk::MemoryPropertyFlags::eHostCoherentBit);
            uniform_buffers.push(uniform_buffer);
            uniform_buffer_memories.push(uniform_buffer_memory);
        }

        (uniform_buffers, uniform_buffer_memories)
    }

    fn create_descriptor_pool(vk: &BaseVulkanObjects, swapchain_images: &Vec<vk::Image>) -> vk::DescriptorPool {
        let pool_sizes = [
            vk::DescriptorPoolSize {
                e_type: vk::DescriptorType::eUniformBuffer,
                descriptor_count: swapchain_images.len() as u32,
            },
            vk::DescriptorPoolSize {
                e_type: vk::DescriptorType::eCombinedImageSampler,
                descriptor_count: swapchain_images.len() as u32,
            }
        ].map(|s| s.into_raw());

        let pool_info = vk::DescriptorPoolCreateInfo {
            flags: Default::default(),
            max_sets: swapchain_images.len() as u32,
            pool_sizes: &pool_sizes[..],
        }.into_raw();

        vk.device.read().create_descriptor_pool(&pool_info).expect("Failed ot create descriptor pool").0
    }

    fn create_descriptor_sets(vk: &BaseVulkanObjects, descriptor_pool: &mut vk::DescriptorPool, descriptor_set_layout: &vk::DescriptorSetLayout, swapchain_images: &Vec<vk::Image>, uniform_buffers: &Vec<vk::Buffer>) -> Vec<vk::DescriptorSet> {
        let layouts = vec![descriptor_set_layout.as_handle(); swapchain_images.len()];

        let mut alloc_info = vk::DescriptorSetAllocateInfo {
            set_layouts: &layouts[..],
        }.into_raw();

        let mut descriptor_sets = descriptor_pool.allocate_descriptor_sets::<Vec<_>>(&mut alloc_info).expect("Failed to allocate descriptor sets").0;

        for (ub, ds) in uniform_buffers.iter().zip(descriptor_sets.iter_mut()) {
            let buffer_infos = [vk::DescriptorBufferInfo {
                buffer: ub.as_handle(),
                offset: 0,
                range: core::mem::size_of::<UniformBufferObject>() as vk::DeviceSize,
            }.into_raw()];

            let image_infos = [vk::DescriptorImageInfo {
                sampler: vk.texture_sampler.as_handle(),
                image_view: vk.texture_image_view.as_handle(),
                image_layout: vk::ImageLayout::eShaderReadOnlyOptimal,
            }.into_raw()];

            let mut descriptor_writes = [
                vk::WriteDescriptorSet {
                    dst_set: ds.as_handle(),
                    dst_binding: 0,
                    dst_array_element: 0,
                    descriptor_type: vk::DescriptorType::eUniformBuffer,
                    image_info: &[],
                    buffer_info: &buffer_infos[..],
                    texel_buffer_view: &[],
                }.into_raw(),
                vk::WriteDescriptorSet {
                    dst_set: ds.as_handle(),
                    dst_binding: 1,
                    dst_array_element: 0,
                    descriptor_type: vk::DescriptorType::eCombinedImageSampler,
                    image_info: &image_infos[..],
                    buffer_info: &[],
                    texel_buffer_view: &[],
                }.into_raw()
            ];

            let mut descriptor_copies: [vk::raw_structs::RawCopyDescriptorSet; 0] = [];

            vk.device.read().update_descriptor_sets(descriptor_writes.as_mut_slice(), descriptor_copies.as_mut_slice());
        }

        descriptor_sets
    }

    fn create_command_buffers(vk: &BaseVulkanObjects, command_pool: &mut vk::CommandPool, swapchain_framebuffers: &Vec<vk::Framebuffer>, render_pass: &vk::RenderPass, swapchain_extent: vk::Extent2D, vertex_buffer: &vk::Buffer, index_buffer: &vk::Buffer, indices: usize, descriptor_sets: &Vec<vk::DescriptorSet>, pipeline_layout: &vk::PipelineLayout, graphics_pipeline: &vk::Pipeline) -> Vec<vk::CommandBuffer> {
        let mut alloc_info = vk::CommandBufferAllocateInfo {
            level: vk::CommandBufferLevel::ePrimary,
            command_buffer_count: swapchain_framebuffers.len() as u32,
        }.into_raw();

        let mut command_buffers = command_pool.allocate_command_buffers::<Vec<_>>(&mut alloc_info).expect("Failed to allocate command buffers").0;

        for (i, (cmd, fb)) in command_buffers.iter_mut().zip(swapchain_framebuffers.iter()).enumerate() {
            let begin_info = vk::CommandBufferBeginInfo {
                flags: Default::default(),
                inheritance_info: None,
            }.into_raw();

            cmd.record_with(command_pool, &begin_info, |mut cmd| {
                let clear_colours = [
                    vk::ClearValue::Colour(vk::ClearColourValue::Float32([0.0, 0.0, 0.0, 1.0]).into_raw()).into_raw(),
                    vk::ClearValue::DepthStencil(vk::ClearDepthStencilValue { depth: 1.0, stencil: 0 }.into_raw()).into_raw()
                ];
                let render_pass_info = vk::RenderPassBeginInfo {
                    render_pass: render_pass.as_handle(),
                    framebuffer: fb.as_handle(),
                    render_area: vk::Rect2D {
                        offset: vk::Offset2D { x: 0, y: 0 },
                        extent: swapchain_extent,
                    },
                    clear_values: &clear_colours[..],
                }.into_raw();

                cmd.begin_render_pass(&render_pass_info, vk::SubpassContents::eInline);

                cmd.bind_pipeline(vk::PipelineBindPoint::eGraphics, graphics_pipeline);

                let viewports = [vk::Viewport {
                    x: 0.0,
                    y: 0.0,
                    width: swapchain_extent.width as f32,
                    height: swapchain_extent.height as f32,
                    min_depth: 0.0,
                    max_depth: 1.0
                }.into_raw()];

                let scissors = [vk::Rect2D {
                    offset: vk::Offset2D { x: 0, y: 0 },
                    extent: swapchain_extent
                }.into_raw()];

                cmd.set_viewport(0, &viewports[..]);
                cmd.set_scissor(0, &scissors[..]);

                let vertex_buffers = [vertex_buffer.as_handle()];
                let offsets = [0];
                cmd.bind_vertex_buffers(0, &vertex_buffers[..], &offsets[..]);

                cmd.bind_index_buffer(index_buffer, 0, vk::IndexType::eUint16);

                let descriptor_sets = [descriptor_sets[i].as_handle()];
                cmd.bind_descriptor_sets(vk::PipelineBindPoint::eGraphics, pipeline_layout, 0, &descriptor_sets[..], &[]);

                cmd.draw_indexed(indices as u32, 1, 0, 0, 0);

                cmd.end_render_pass();

                Ok(())
            }).expect("Failed to record command buffer");
        }

        command_buffers
    }

    fn create_sync_objects(vk: &BaseVulkanObjects, swapchain_images: &Vec<vk::Image>) -> ([vk::Semaphore; HelloTriangleApplicationData::MAX_FRAMES_IN_FLIGHT], [vk::Semaphore; HelloTriangleApplicationData::MAX_FRAMES_IN_FLIGHT], [vk::Fence; HelloTriangleApplicationData::MAX_FRAMES_IN_FLIGHT], Vec<Option<vk::Fence>>) {
        let semaphore_create_info = vk::SemaphoreCreateInfo {
            flags: Default::default(),
        }.into_raw();

        let fence_create_info = vk::FenceCreateInfo {
            flags: vk::FenceCreateFlags::eSignaledBit,
        }.into_raw();

        let image_available_semaphores = array_init::array_init(|_| vk.device.read().create_semaphore(&semaphore_create_info).expect("Failed to create semaphore").0);
        let render_finished_semaphores = array_init::array_init(|_| vk.device.read().create_semaphore(&semaphore_create_info).expect("Failed to create semaphore").0);

        let in_flight_frames = array_init::array_init(|_| vk.device.read().create_fence(&fence_create_info).expect("Failed to create fence").0);

        let images_in_flight = (0..swapchain_images.len()).map(|_| None).collect();

        (image_available_semaphores, render_finished_semaphores, in_flight_frames, images_in_flight)
    }

    fn draw_frame(&mut self) -> MainLoopResult {
        if vk::SuccessCode::eNotReady == self.in_flight_frames[self.current_frame].read().get_fence_status().expect("Failed to query fence status") {
            // println!("\tIn flight skip");
            return MainLoopResult::Skipped;
        }
        // self.device.wait_for_fences(&fences[..], true, u64::MAX).expect("Failed to wait on fence");

        let image_index = if let Some(image_index) = self.acquired.take() {
            image_index
        } else {
            match self.swapchain.write().acquire_next_image_khr(u64::MAX, Some(&mut self.image_available_semaphores[self.current_frame]), None) {
                Ok((image_index, _)) => {
                    image_index
                },
                Err(e) => match e {
                    vk::ErrorCode::eErrorOutOfDateKhr => { return MainLoopResult::OutdatedSwapchain; }
                    e => Err(e).expect("Failed to acquire image")
                }
            }
        };

        if let Some(image_in_flight) = &self.images_in_flight[image_index as usize] {
            // let fences = [image_in_flight.as_handle()];
            // self.device.wait_for_fences(&fences[..], true, u64::MAX).expect("Failed to wait on fence");
            if vk::SuccessCode::eNotReady == image_in_flight.read().get_fence_status().expect("Failed to query fence status") {
                self.acquired = Some(image_index);
                // println!("\tImage in use skip");
                return MainLoopResult::Skipped;
            }
        }
        self.images_in_flight[image_index as usize].replace(self.in_flight_frames[self.current_frame].clone());

        self.update_uniform_buffer(image_index);

        let wait_semaphores = [self.image_available_semaphores[self.current_frame].as_handle()];
        let wait_stages = [vk::PipelineStageFlags::eColourAttachmentOutputBit];
        let command_buffets = [self.command_buffers[image_index as usize].as_handle()];
        let signal_semaphores = [self.render_finished_semaphores[self.current_frame].as_handle()];

        let submit_infos = [vk::SubmitInfo {
            wait_semaphores: &wait_semaphores[..],
            wait_dst_stage_mask: &wait_stages[..],
            command_buffers: &command_buffets[..],
            signal_semaphores: &signal_semaphores[..],
        }.into_raw()];

        {
            let mut fence_write_lock = self.in_flight_frames[self.current_frame].write();
            let mut fences = [fence_write_lock.as_mut_handle()];
            self.device.read().reset_fences(&mut fences[..]).expect("Failed to reset fence");
        }
        self.graphics_queue.write().queue_submit(&submit_infos[..], Some(self.in_flight_frames[self.current_frame].write().deref_mut())).expect("Failed to submit command buffer");

        let mut swapchain = self.swapchain.write();
        let mut swapchains = [swapchain.as_mut_handle()];
        let image_indices = [image_index];

        let mut signal_semaphores = [self.render_finished_semaphores[self.current_frame].as_mut_handle()];
        let mut present_info = vk::PresentInfoKHR {
            wait_semaphores: &mut signal_semaphores[..],
            swapchains: &mut swapchains[..],
            image_indices: &image_indices[..],
            results: &mut [],
        }.into_raw();

        let pt = Instant::now();
        match self.base_objects.present_queue.write().queue_present_khr(&mut present_info) {
            Ok(r) => if r == vk::SuccessCode::eSuboptimalKhr { return MainLoopResult::OutdatedSwapchain; },
            Err(r) => if r == vk::ErrorCode::eErrorOutOfDateKhr { return MainLoopResult::OutdatedSwapchain; } else { Err(r).expect("Failed to present") }
        }
        vk::flush_window_compositor();

        if self.present_times.len() == Self::MAX_FRAME_TIMES {
            self.present_times.pop_back();
        }
        self.present_times.push_front(pt);

        self.current_frame += 1;
        self.current_frame %= Self::MAX_FRAMES_IN_FLIGHT;

        MainLoopResult::Success
    }

    fn recreate_swapchain(&mut self) -> bool {
        if self.last_window_size.width == 0 || self.last_window_size.height == 0 {
            return true;
        }

        if !self.recreating_swapchain {

            self.recreation_request_sender.send((
                self.base_objects.clone(),
                SwapchainRecreationRequest {
                    command_pool: self.command_pool.clone(),
                    surface: self.surface.clone(),
                    window_size: self.last_window_size,
                    old_swapchain_format: self.swapchain_image_format,
                    old_depth_format: self.depth_format,
                    old_renderpass: self.render_pass.clone(),
                    old_swapchain_image_count: self.swapchain_images.len(),
                    old_descriptor_sets: self.descriptor_sets.clone(),
                    old_swapchain: self.swapchain.clone(),
                    old_pipeline_layout: self.pipeline_layout.clone(),
                    old_graphics_pipeline: self.graphics_pipeline.clone()
                }
            )).unwrap();

            self.recreating_swapchain = true;

            true
        } else {
            if let Ok(r) = self.recreation_response_receiver.try_recv() {

                self.command_pool.write().free_command_buffers(self.command_buffers.drain(0..));

                self.swapchain = Arc::new(RwLock::new(r.swapchain));
                self.swapchain_image_format = r.swapchain_image_format;
                self.swapchain_extent = r.swapchain_extent;
                self.swapchain_images = r.swapchain_images;
                self.swapchain_image_views = r.swapchain_image_views;
                self.render_pass = r.render_pass;
                if let Some(pipeline_layout) = r.pipeline_layout {
                    self.pipeline_layout = pipeline_layout;
                }
                if let Some(graphics_pipeline) = r.graphics_pipeline {
                    self.graphics_pipeline = graphics_pipeline;
                }
                self.colour_image = r.colour_image;
                self.colour_image_memory = r.colour_image_memory;
                self.colour_image_view = r.colour_image_view;
                self.depth_image = r.depth_image;
                self.depth_image_memory = r.depth_image_memory;
                self.depth_image_view = r.depth_image_view;
                self.depth_format = r.depth_format;
                self.swapchain_framebuffers = r.swapchain_framebuffers;
                if let Some(uniform_buffers) = r.uniform_buffers {
                    self.uniform_buffers = uniform_buffers;
                }
                if let Some(uniform_buffer_memories) = r.uniform_buffer_memories {
                    self.uniform_buffer_memories = uniform_buffer_memories;
                }
                if let Some(descriptor_pool) = r.descriptor_pool {
                    self.descriptor_pool = descriptor_pool;
                }
                if let Some(descriptor_sets) = r.descriptor_sets {
                    self.descriptor_sets = Arc::new(descriptor_sets);
                }
                self.command_buffers = r.command_buffers;
                if let Some(image_available_semaphores) = r.image_available_semaphores {
                    self.image_available_semaphores = image_available_semaphores;
                }
                if let Some(render_finished_semaphores) = r.render_finished_semaphores {
                    self.render_finished_semaphores = render_finished_semaphores;
                }
                if let Some(in_flight_frames) = r.in_flight_frames {
                    self.in_flight_frames = in_flight_frames.map(|f| Arc::new(RwLock::new(f)));
                }
                if let Some(images_in_flight) = r.images_in_flight {
                    self.images_in_flight = images_in_flight.into_iter().map(|f| f.map(|f| Arc::new(RwLock::new(f)))).collect();
                }

                self.recreating_swapchain = false;

                false
            } else {
                true
            }
        }
    }

    fn update_uniform_buffer(&mut self, image_index: u32) {
        let time = self.start_time.elapsed().as_secs_f32();
        let aspect = self.last_window_size.width as f32 / self.last_window_size.height as f32;

        let ubo = UniformBufferObjectInit {
            // model: psm::Matrix4f::identity(),
            model: psm::Matrix4f::rotate_y(psm::Degrees::new(45.0 * time)) * psm::Matrix4f::rotate_x(psm::Degrees::new(90.0)),
            // view: psm::Matrix4f::identity(),
            view: psm::Matrix4f::space_look_at(psm::Vector3f::new(2.0, -2.0, 2.0), psm::Vector3f::zero(), -psm::UnitVector3f::unit_y(), NdcConfig::VULKAN),
            proj: psm::Matrix4f::perspective_projection(psm::Degrees::new(45.0), aspect, ControlAxis::Vertical, DepthDomain::Finite { near: 0.1, far: 10.0 }, ForwardDirection::VULKAN, DepthRange::VULKAN),
        }.init();

        let mut data = self.uniform_buffer_memories[image_index as usize].map_memory(0, std::mem::size_of::<UniformBufferObject>() as vk::DeviceSize, vk::MemoryMapFlags::eNone).expect("Failed to map staging index memory").0;
        let ubo_bytes = unsafe { std::slice::from_raw_parts(&ubo as *const _ as *const u8, std::mem::size_of::<UniformBufferObject>()) };
        data.copy_from_slice(ubo_bytes);
        self.uniform_buffer_memories[image_index as usize].unmap_memory(data);
    }

    //

    fn check_validation_layer_support<'a, 'b, 'c: 'b, I>(&'a self, layers: I) -> bool where I: IntoIterator<Item=&'b vk::UnsizedCStr<'c>> {
        let available_layers = self.loader.enumerate_instance_layer_properties::<Vec<_>>().expect("Failed to enumerate instance layers").0
            .iter().map(|l| l.layer_name.to_c_string().unwrap()).collect::<HashSet<_>>();

        layers.into_iter().all(|l| available_layers.contains(l.as_cstr()))
    }

    fn list_required_instance_extensions(window: &Window) -> Vec<vk::UnsizedCStr<'static>> {
        vk::get_required_surface_extensions(window).expect("Not a supported platform").into_iter()
            .chain(Self::DEBUG_EXTENSIONS.iter().copied())
            .collect()
    }

    extern "C" fn debug_callback(message_severity: vk::raw_bit_fields::RawDebugUtilsMessageSeverityFlagBitsEXT, message_types: vk::DebugUtilsMessageTypeFlagsEXT, callback_data: Option<&vk::raw_structs::RawDebugUtilsMessengerCallbackDataEXT>, _p_user_data: Option<&'static ()>) -> vk::Bool32 {
        let message_severity = message_severity.normalise();
        if let Some(callback_data) = callback_data {
            if message_severity != vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerboseBit {
                let msg = callback_data.message.as_str().unwrap();
                if !msg.contains("wrong ELF class") && !msg.contains("Failed to open dynamic library") {
                    println!("Validation layer: {:?} - {:?}\n\tP{}", message_severity, message_types, msg);
                }
            }
        }

        false.into()
    }

    fn debug_messenger_create_info() -> vk::DebugUtilsMessengerCreateInfoEXT {
        vk::DebugUtilsMessengerCreateInfoEXT {
            flags: Default::default(),
            message_severity: vk::DebugUtilsMessageSeverityFlagBitsEXT::eErrorBit | vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarningBit | vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerboseBit | vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfoBit,
            message_type: vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformanceBit | vk::DebugUtilsMessageTypeFlagBitsEXT::eValidationBit | vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneralBit,
            pfn_user_callback: Self::debug_callback,
            user_data: None,
        }
    }

    fn list_required_device_extensions(&self) -> Vec<vk::UnsizedCStr<'static>> {
        vec![vk::KHR_SWAPCHAIN_EXTENSION_NAME_UNSIZED_CSTR]
    }
}
