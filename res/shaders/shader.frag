#version 450

layout(binding = 1) uniform sampler2D tex_sampler;

layout(location = 0) in vec2 tex_coords;

layout(location = 0) out vec4 out_colour;

void main() {
//    out_colour = vec4(tex_coords, 0.0, 1.0);
    out_colour = texture(tex_sampler, tex_coords);
}
